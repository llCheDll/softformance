import ipdb

from django.http import HttpResponse
from .models import RegisteredUser, FeedItem


def index(request):
    my_posts = FeedItem.objects.all().filter(user__id=request.user.id)
    subscribes = RegisteredUser.objects.filter(tracked_by__user_id = request.user.id)
    posts = FeedItem.objects.all()
    
    my_and_sub_posts = []
    for sub in subscribes:
        my_and_sub_posts += list(FeedItem.objects.filter(user__id=sub.user.id))
        
    output = '<div class="row">{}</div>'
    column = '<div class="column">{}</div>'
    
    my_posts_output = '<h2>My posts</h2><br>' + '<br> '.join([p.content for p in my_posts])
    my_and_subscribes = '<h2>Subscribes and Me</h2><br>' + '<br> '.join(
        [sub.content for sub in my_and_sub_posts]
    )
    all_posts =  '<h2>All posts</h2><br>' + '<br> '.join(
        [post.content for post in posts]
    )
    
    columns = ''.join(
        [column.format(my_posts_output), column.format(my_and_subscribes), column.format(all_posts)]
    )
    
    output = output.format(columns)
    return HttpResponse(output)
