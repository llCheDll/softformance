from django.contrib import admin
from django.urls import path, include

from core.rest import views



urlpatterns = [
    path('', include('blog.urls')),
    path('admin/', admin.site.urls),
    path('users/', views.UserListAPIView.as_view(), name='users'),
    path('loginOG/', views.OGLoginAPIView.as_view(), name='loginOG'),
    path('login/', views.LoginAPIView.as_view(), name='login'),
    path('logout/', views.LogoutAPIView.as_view(), name='logout'),
]
