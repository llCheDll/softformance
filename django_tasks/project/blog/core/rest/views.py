import json
import jwt
import os
import requests
import hashlib
import urllib
import ipdb

from django.contrib.auth import authenticate, login, logout
from django.contrib.auth.models import User
from django.http import HttpResponseRedirect

from rest_framework import exceptions
from rest_framework.authentication import SessionAuthentication, BasicAuthentication
from rest_framework.reverse import reverse
from rest_framework.views import APIView
from rest_framework.generics import ListAPIView
from rest_framework import permissions

from core.rest.serializers import UserSerializer
from core.settings import OG_AUTH


class UserListAPIView(ListAPIView):
    queryset = User.objects.all()
    serializer_class = UserSerializer
    authentication_classes = [SessionAuthentication, BasicAuthentication]
    permission_classes = [permissions.IsAuthenticated]


class LoginAPIView(APIView):
    def post(self, request):
        username = request.POST.get('username')
        password = request.POST.get('password')

        user = authenticate(request, username=username, password=password)

        if user is not None and password != '':
            login(request, user)
            return HttpResponseRedirect(reverse('users', request=request))
        else:
            raise exceptions.AuthenticationFailed('No such user or password don`t match')


class OGLoginAPIView(APIView):
    def get(self, request):
        if 'code' not in request.query_params:
            state = hashlib.sha256(os.urandom(1024)).hexdigest()
            request.session['state'] = state

            params = {
                'response_type': 'code',
                'client_id': OG_AUTH['ClientID'],
                'redirect_uri': OG_AUTH['RedirectURL'],
                'scope': 'openid email',
                'state': state,
            }

            redirect_url = OG_AUTH['authorizeURL'] + '?' + urllib.parse.urlencode(params)
            return HttpResponseRedirect(redirect_to=redirect_url)
        else:
            if request.query_params.get('state', '') == request.session.get('state'):
                auth_code = request.query_params['code']

                data = {'code': auth_code,
                        'client_id': OG_AUTH['ClientID'],
                        'client_secret': OG_AUTH['ClientSecret'],
                        'redirect_uri': OG_AUTH['RedirectURL'],
                        'grant_type': 'authorization_code'}

                r = requests.post(OG_AUTH['tokenURL'], data=data)
                token = json.loads(r.text)['id_token']

                user_info = jwt.decode(token, options={"verify_signature": False})

                if not User.objects.filter(username=user_info['email']).exists():
                    register = User.objects.create_user(
                        username=user_info['email'],
                        password=''
                    )
                    register.save()

                user = authenticate(request, username=user_info['email'], password='')

                if user is not None:
                    if user.is_active:
                        login(request, user)

                return HttpResponseRedirect(reverse('users', request=request))
            else:
                raise exceptions.AuthenticationFailed('No such user or password don`t match')


class LogoutAPIView(APIView):
    def get(self, request):
        logout(request)
        return HttpResponseRedirect(reverse('users', request=request))
