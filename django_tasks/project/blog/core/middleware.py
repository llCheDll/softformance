from django.http import HttpResponseRedirect, HttpRequest


SSL = 'SSL'


def request_is_secure(request):
    if request.is_secure():
        return True
    return False


class SSLCheck:
    def __init__(self, get_response):
        self._get_response = get_response

    def __call__(self, request):
        self.process_request(request)
        response = self._get_response(request)
        return response
        
    def process_request(self, request):
        if request_is_secure(request):
            request.IS_SECURE=True
        return None

    def process_view(self, request, view_func, view_args, view_kwargs):
        if SSL in view_kwargs:
            view_secure = view_kwargs[SSL]
            del view_kwargs[SSL]
        else:
            view_secure = False
            
        if view_secure:
            if not request_is_secure(request):
                return self._redirect(request)

    def _redirect(self, request):
        newurl = "https://{}{}".format(HttpRequest.get_host(request), request.get_full_path())
        return HttpResponseRedirect(newurl)
