import re

from code import InteractiveConsole


def password_check(passwords):
    result = []
    
    for password in passwords:
        if re.match('\w\S+[^\s]', password) and 4 <= len(password) <= 6:
            result.append(password)
    
    return ','.join(result)


if __name__ == '__main__':
    data = InteractiveConsole.raw_input('Input :')
    data = data.split(',')
    print(password_check(data))
