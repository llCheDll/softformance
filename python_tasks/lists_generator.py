from code import InteractiveConsole

def listGenerator(lists_quantity, values_quantity):
    tmp_lst = [counter for counter in range(1, lists_quantity*values_quantity+1)]
    result = []
    
    counter = 0
    
    for i in range(0, lists_quantity):
        result.append(tmp_lst[counter:counter+values_quantity])
        counter += values_quantity

    return result


if __name__ == '__main__':
    data = InteractiveConsole.raw_input('Prompt :')
    data = data.split(',')
    print(listGenerator(int(data[0]),int(data[1])))
