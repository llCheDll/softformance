#https://weasyprint.readthedocs.io/en/stable/install.html
#to use cairosvg need to install:
#   for MAC: brew install python3 cairo pango gdk-pixbuf libffi
#   for UBUNTU: sudo apt-get install build-essential python3-dev python3-pip python3-setuptools python3-wheel python3-cffi libcairo2 libpango-1.0-0 libpangocairo-1.0-0 libgdk-pixbuf2.0-0 libffi-dev shared-mime-info

import cairosvg
import re
import urllib.request
import urllib.parse

from bs4 import BeautifulSoup as bs
from PIL import Image
from os.path import os


class ImageCrawler:
    def __init__(self, url):
        try:
            with urllib.request.urlopen(url) as req:
                content = req.read().decode('utf-8')
                soup = bs(content, 'html.parser')
        except urllib.request.HTTPError as error:
            raise ValueError('HTTPError:', url)
        
        url_parts = urllib.parse.urlparse(url)
        
        self.images_path = os.getcwd() + '/images/' + url_parts.netloc + '/'
        
        flag = 0
        
        for path in url_parts.path.split('/'):
            if path:
                self.images_path += path + '/'
                if not os.path.isdir(self.images_path):
                    flag = 1
                    os.mkdir(self.images_path)
                    
        if not flag:
            if not os.path.isdir(self.images_path):
                os.mkdir(self.images_path)
        
        self.base_url = url_parts.scheme + '://' + url_parts.netloc
        self.soup = soup
        self.headers = {'User-Agent':'Mozilla/5.0 (Macintosh; Intel Mac OS X 10_10_1) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/39.0.2171.95 Safari/537.36'}
        self.url = url
        self.image_pattern = "\\bsrc?\w+=\"(.*?)\""
        self.crawled_urls = []

    def prepare_img_url(self, image_url):
        result = []
        
        image_url = image_url.split(',')
        
        for url in image_url:
            url = url.strip()
            url = url.split(' ')[0]
            
            if url:
                if 'https://' not in url and 'http://' not in url:
                    if '//' in url:
                        result.append('https:'+url)
                    else:
                        result.append(self.base_url+url)
                else:
                    result.append(url)

        return result

    def get_urls(self):
        img_tags = self.soup.find_all('img')
        picture_tags = self.soup.find_all('picture')
        
        urls = re.findall(self.image_pattern, str(img_tags)+str(picture_tags))
        
        prepared_urls = []
        
        for url in urls:
            prepared_urls += self.prepare_img_url(url)
            
        return prepared_urls

    def download_images(self, url):
        url_parse = urllib.parse.urlparse(url)
        image_name = url_parse.path.split('/')[-1]
        url_parse = url_parse.scheme +'://' + url_parse.netloc + urllib.parse.quote(url_parse.path)
        
        try:
            img = urllib.request.urlopen(url_parse)
            with open(self.images_path + image_name, 'wb') as file:
                file.write(img.read())
        except urllib.request.HTTPError as error:
            print(error)
            print(url)
        except Exception as error:
            print(error)
            print(url)
            

    def get_images(self):
        images = self.get_urls()
        
        for image in images:
            self.download_images(image)

    def to_png(self):
        images = os.listdir(self.images_path)
        print(self.images_path)
        for image in images:
            if image != '.DS_Store':
                image_name = image.split('.')
                
                if os.path.isfile(self.images_path + image):
                    if image_name[-1] == 'svg':
                            cairosvg.svg2png(
                                url=(self.images_path + image),
                                write_to=(self.images_path + image_name[0] + '.png')
                            )
                            os.remove(self.images_path + image)
                    elif image_name[-1] not in 'png':
                        try:
                            with Image.open(self.images_path + image) as img:
                                img.save(self.images_path + image_name[0] + '.png', 'png')
                            os.remove(self.images_path + image)
                        except OSError:
                            print("cannot convert", self.images_path + image)
                            os.remove(self.images_path + image)
    
    def website_walk(self):
        sub_pages = self.soup.find_all('a', href=True)
        sub_pages_urls = []
        
        for href in sub_pages:
            if href['href'].startswith(self.base_url):
                sub_pages_urls.append(href['href'])
        
        return sub_pages_urls


urls = []

def run(url):
    try:
        obj = ImageCrawler(url)
        obj.get_images()
        obj.to_png()
        urls.append(url)
        
        for url in obj.website_walk():
            if url not in urls and 'ru' not in url and 'en' not in url:
                urls.append(url)
                run(url)
    except ValueError as e:
        print(e)

if __name__ == '__main__':
    url = 'https://pomidoros-pizza.com.ua/'
    run(url)
